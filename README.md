An Arc theme for Gedit text editor.

The original Arc project is here https://github.com/horst3180/Arc-theme

**Installation :**

Copy the arc.xml to the color schemes directory:
~/.local/share/gedit/styles **or** ~/.local/share/gtksourceview-3.0/styles

    cd /tmp && wget https://framagit.org/fmr/Gedit_Arc_theme/-/archive/master/Gedit_Arc_theme-master.tar && tar xvf Gedit_Arc_theme-master.tar && Gedit_Arc_theme-master && mv arc.xml $HOME/.local/share/gedit/styles/


**Activation :**

- Open the Preferences in Gedit 
- Switch to the "Font & Colors" tab 
- Select Arc from the "Color Scheme" list

![arc_theme_gedit](/uploads/fbfaaf6ed381a1a6d7e22cad1c517006/arc_theme_gedit.png)